import sys
import json

def threebytes_to_address(a):
    assert len(a) == 3
    snes_address = a[0] + 256 * a[1] + 65536 * a[2]
    rom_address = (snes_address // 65536) * 32768 + (snes_address % 32768)
    return {
        'snes': snes_address,
        'snes_hex': hex(snes_address),
        'rom': rom_address,
        'rom_hex': hex(rom_address),
    }


def get_info(b):
    # Referenced here: https://github.com/brunovalads/yoshisisland-disassembly/blob/8fa1ada54aa99d8d89a32575c7c9ff7cd06ed957/disassembly/bank01.asm#L5822-L5837
    start = 0xbf7c3
    count = 0xde
    dat = []
    for i in range(count):
        c = b[start + 6 * i:start + 6 * i + 6]
        me = {
            'id': hex(i),
            'object': threebytes_to_address(c[:3]),
            'sprite': threebytes_to_address(c[3:]),
        }
        dat.append(me)
    return dat


def main():
    filename = sys.argv[1]
    with open(filename, 'rb') as fp:
        content = fp.read()
        result = get_info(content)
        print(json.dumps(result, indent=2))


if __name__ == '__main__':
    main()

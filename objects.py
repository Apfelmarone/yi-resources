import sys
import json
import stages

# Ref: https://github.com/brunovalads/yoshisisland-disassembly/blob/master/scripts/dump_levels.py

objects_info = [""] * 512
objects_len = [None] * 512 # How many bytes does an object take?

# Ad-hoc initialization
objects_info[0x68] = "coins"
objects_len[0x68] = 5
objects_info[0x6b] = "Goal platform"
objects_len[0x6b] = 5
objects_info[0x9e] = "Donut lift"
objects_len[0x9e] = 4
objects_info[0xc4] = "horizontal coins"
objects_len[0xc4] = 4
objects_info[0xc6] = "diagonal coins"
objects_len[0xc6] = 4
for i in range(0xe4,0xed):
    objects_info[i] = "Flower-related ledge?"
    objects_len[i] = 5


def get_objects(rom, start):
    pos = start
    while rom[pos] != 0xff or rom[pos + 1] != 0xff:
        num = rom[pos]
        if objects_len[num] is None:
            assert False, f"{hex(pos)}, {hex(num)}"
        print(hex(pos))
        print([hex(x) for x in rom[pos:pos+objects_len[num]]])
        pos += objects_len[num]
    return None

def main():
    filename = sys.argv[1]
    with open(filename, 'rb') as fp:
        content = fp.read()
        result = stages.get_info(content)
        addr = result[4]['object']['rom']
        result = get_objects(content, addr)
        print(json.dumps(result, indent=2))


if __name__ == '__main__':
    main()

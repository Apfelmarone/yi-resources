import sys
import json
import stages

sprite_info = [""] * 512

# Ad-hoc initialization
sprite_info[0xd] = "Goal"
sprite_info[0x65] = "Red coin"
sprite_info[0xfa] = "Flower"
sprite_info[0x12c] = "Fly guy w/ red coin or whirly fly guy w/ 1up"
for i in range(0x1ca, 0x1d5):
    sprite_info[i] = "Some scrolling-related sprite"

def get_sprites(rom, start):
    # For level 0x4
    result = []
    pos = start
    while rom[pos] != 0xff:
        b = rom[pos:pos + 3]
        num = b[0]
        x = b[2]
        y = b[1] // 2
        if b[1] % 2 == 1:
            num += 256
        entry = {
            "addr": hex(pos),
            "id": hex(num),
            "desc": sprite_info[num],
            "x": hex(x),
            "y": hex(y),
        }
        result.append(entry)
        pos += 3
    return result

def main():
    filename = sys.argv[1]
    with open(filename, 'rb') as fp:
        content = fp.read()
        result = stages.get_info(content)
        addr = result[4]['sprite']['rom']
        result = get_sprites(content, addr)
        print(json.dumps(result, indent=2))


if __name__ == '__main__':
    main()
